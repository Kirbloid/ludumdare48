extends Node2D

enum NPC { CHARLES, EVONN, RACHEL, TRENT, LESLEA }

onready var game := get_node("/root/Main/Scenes/Game")

var current_npc = NPC.LESLEA

# Memory states
var found_101 = false
var found_201 = false
var found_301 = false
var found_401 = false
var found_501 = false
var found_601 = false

func _ready() -> void:
	pass


func handle_memory_captured(memory_id : int, npc : int) -> void:
	current_npc = npc
	match memory_id:
		0:
			if current_npc == NPC.LESLEA:
				memory_01()
			else:
				memory_00()
		2:
			memory_02() # HP runout
		3:
			memory_03() # MP runout
		4:
			memory_04() # Reached the end
		101:
			memory_101()
		201:
			memory_201()
		301:
			memory_301()
		401:
			memory_401()
		501:
			memory_501()
		601:
			memory_601()
		500:
			memory_500() # Leslea
		777:
			memory_777() # Ending
		1001:
			memory_1001() # Debug


func memory_00() -> void:
	# Default memory result for when a non-helpful memory is captured
	# by someone other than Leslea
	var story = [
		{
			"Player": true,
			"Dialog": "No, that's not it. There's got to be another memory " + \
			"that will help me dive a bit deeper into all this...\n\n " + \
			"Either need to try again or dive into someone else's memory."
		}
	]
	game.start_dialog(story, current_npc)


func memory_01() -> void:
	# Default memory result for pretty much any captured memory in Leslea
	# because there's only 1 at the bottom we're looking for
	var story = [
		{
			"Player": true,
			"Dialog": "Not diving deep enough. I know the memory I need " + \
			"is here but this isn't what I'm looking for.\n\n " + \
			"It has to be the last thing Leslea remembers. If not... " + \
			"I don't know what I'd do if we can't find it."
		}
	]
	game.start_dialog(story, current_npc)


func memory_02() -> void:
	# Ran out of HP
	var story = [
		{
			"Player": true,
			"Dialog": "Ouch! That was a bit much. I know some memories can " + \
			"hurt but this is rediculous.\n\n " + \
			"Either need to raise my diving level to increase my Hit Points " + \
			"or just dodge a bit more when I can."
		}
	]
	game.start_dialog(story, current_npc)


func memory_03() -> void:
	# Ran out of MP
	var story = [
		{
			"Player": true,
			"Dialog": "Couldn't dive deep enough without running out of " + \
			"energy first.\n\n " + \
			"Either need to raise my diving level to increase my Mental Points " + \
			"or be careful about dodging too much. Dive Level 4 should be " + \
			"enough to get where I need to go, but higher won't hurt."
		}
	]
	game.start_dialog(story, current_npc)


func memory_04() -> void:
	# Reached the end of the memories
	var story = [
		{
			"Player": true,
			"Dialog": "That's it for their memories. One of these memories " + \
			"still has to be useful or point me in the right direction.\n\n " + \
			"I should definitely try again."
		}
	]
	game.start_dialog(story, current_npc)


func memory_101() -> void:
	# Starts the game proper
	if not found_101:
		var story = [
			{
				"Player": true,
				"Dialog": "A good start, but not enough to solve this. " + \
				"Whatever happened is locked deep within Leslea's " + \
				"memories. I'm going to have to try again after synchronizing " + \
				"with the memories of her friends. " + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_101 = true
		game.level_up()
		game.assign_friend_memories()
		game.ui.update_default_thoughts("So... Leslea and their friends went camping over the weekend. Let's see what they can add and level up the DIVE on the way.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_201() -> void:
	# Starts the game proper
	if not found_201:
		var story = [
			{
				"Player": true,
				"Dialog": "The drinking might explain some of the instability " + \
				"I'm seeing as I try to sync. If it goes too far... I'm not " + \
				"sure this dive will even be successful. " + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_201 = true
		game.level_up()
		game.ui.update_default_thoughts("Trent convinces Leslea to drink with him, Charles, and Rachel, but Evonn is sitting out.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_301() -> void:
	# Starts the game proper
	if not found_301:
		var story = [
			{
				"Player": true,
				"Dialog": "Good, stopped the drinking before it impacted " + \
				"things a bit too much. This should mean her memory's pretty " + \
				"intact up until the moment I need. " + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_301 = true
		game.level_up()
		game.ui.update_default_thoughts("Leslea slows down with the drinking to console Evonn who's moving away. Rachel joins them.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_401() -> void:
	# Starts the game proper
	if not found_401:
		var story = [
			{
				"Player": true,
				"Dialog": "I'm starting to worry this isn't going to " + \
				"get me where I need to go, but it's the only lead I have " + \
				"at this point. " + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_401 = true
		game.level_up()
		game.ui.update_default_thoughts("Rachel teased that Charles is saying too much and making her uncomfortable, Trent egging him on. Charles started hitting on Leslea.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_501() -> void:
	# Starts the game proper
	if not found_501:
		var story = [
			{
				"Player": true,
				"Dialog": "Dammit. Leslie was seperated from the entire group." + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_501 = true
		game.level_up()
		game.ui.update_default_thoughts("Charles pushes too far, and Trent, defending Leslea, gets in a fight with him. Leslea is last seen walking off to a nearby stream to let them cool down.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_601() -> void:
	# Starts the game proper
	if not found_601:
		var story = [
			{
				"Player": true,
				"Dialog": "If... if this is true, then only Leslea has the" + \
				"final piece of all of this." + \
				"\n\n " + \
				" -- DIVE Level Up -- "
			}
		]
		found_601 = true
		game.level_up()
		game.ui.update_default_thoughts("The group found Leslea collapsed nearby, the doctors discovering bite marks on her ankle.")
		game.start_dialog(story, current_npc)
	else:
		memory_888()


func memory_500() -> void:
	if not found_101:
		var story = [
			{
				"Player": true,
				"Dialog": "At least I know the system works. Now I just need " + \
				"to capture a memory that syncs up with Leslea better and " + \
				"level up the DIVE."
			}
		]
		game.start_dialog(story, current_npc)
	else:
		memory_00()


func memory_777() -> void:
	# Wins the game
	# The final memory in Leslea
	var story = [
		{
			"Player": true,
			"Dialog": "It was a Western Diamondback rattlesnake... Leslea... hold on."
		}
	]
	game.start_dialog(story, current_npc)
	game.end_flag = true


func memory_888() -> void:
	# Repeats
	var story = [
		{
			"Player": true,
			"Dialog": "I already found this memory. It's not going to " + \
			"level up my DIVE anymore."
		}
	]
	game.start_dialog(story, current_npc)


func memory_1001() -> void:
	# Testing function
	var story = [
		{
			"Player": true,
			"Dialog": "-- Level up debug --"
		}
	]
	game.level_up()
	game.start_dialog(story, current_npc)
