extends Node2D

enum State { WALKING, TALKING, DIVING }

var _ending = preload("res://Scenes/Ending.tscn")
var _memory = preload("res://Scenes/Memories/Memory.tscn")
var _current_state = State.WALKING setget set_state, get_state
var _current_dive

onready var main = get_node("/root/Main")
onready var player = get_node("Player")
onready var dive_holder = get_node("UI/Dive")
onready var ui = get_node("UI")
onready var story_manager = get_node("StoryManager")

var rng = RandomNumberGenerator.new()
var end_flag = false

var player_diver
var hp = 50
var hp_max = 50
var mp = 100
var mp_max = 100
var dive_level = 0


func _ready() -> void:
	rng.randomize()
	ui.update_stats()


func _process(delta) -> void:
	match get_state():
		State.WALKING:
			get_walking_input()
		State.TALKING:
			get_talking_input()
		State.DIVING:
			get_diving_input()


func set_state(state: int) -> void: #state = enum State
	_current_state = state


func get_state() -> int:
	return _current_state


func get_walking_input() -> void:
	player.velocity = Vector2()
	
	if Input.is_action_pressed("ui_right"):
		player.move_right()
	elif Input.is_action_pressed("ui_left"):
		player.move_left()
	else:
		player.moving_x = false
	
	if Input.is_action_pressed("ui_down"):
		player.move_down()
	elif Input.is_action_pressed("ui_up"):
		player.move_up()
	else:
		player.moving_y = false
	
	if Input.is_action_just_pressed("z"):
		var dialog = player.get_talk_interaction()
		if dialog != null:
			var story_instance = dialog.instance()
			start_dialog(story_instance.story, player.get_npc())
	
	if Input.is_action_just_pressed("x"):
		var dive = player.get_dive_interaction()
		if dive != null:
			start_memory_dive(dive)


func get_talking_input() -> void:
	if Input.is_action_just_pressed("z"):
		ui.advance_dialog()
		if end_flag == true:
			player.set_visible(false)
			$UI/Dialog/Narrative.set_visible(false)
			end_game()


func get_diving_input() -> void:
	player_diver.velocity = Vector2()
	
	if Input.is_action_pressed("ui_right"):
		player_diver.move_right()
	elif Input.is_action_pressed("ui_left"):
		player_diver.move_left()
	else:
		player_diver.moving_x = false
	
	if Input.is_action_pressed("ui_down"):
		player_diver.move_down()
	elif Input.is_action_pressed("ui_up"):
		player_diver.move_up()
	else:
		player_diver.moving_y = false
	
	if Input.is_action_just_pressed("z"):
		player_diver.dash()
	
	if Input.is_action_just_pressed("x"):
		_current_dive.capture_memory()
	
	if Input.is_action_pressed("ui_cancel"):
		end_dive(0)


func start_dialog(dialog : Array, npc : int) -> void:
	player.stop()
	print ("Dialog is started containing: " + str(dialog))
	set_state(State.TALKING)
	ui.start_dialog(dialog, npc)


func start_memory_dive(dive_story : PackedScene) -> void:
	$Music.stop()
	player.stop()
	print ("Memory dive is started containing: " + str(dive_story))
	var dive_instance = _memory.instance()
	dive_holder.add_child(dive_instance)
	_current_dive = dive_holder.get_child(0)
	player_diver = _current_dive.get_node("PlayerDive")
	_current_state = State.DIVING
	_current_dive.start_dive(dive_story)
	ui.start_dive(player.get_npc())


func end_dive(memory_id : int) -> void:
	$Music.play()
	set_state(State.TALKING)
	story_manager.handle_memory_captured(memory_id, player.get_npc())
	_current_dive.queue_free()
	reset_stats()
	ui.end_dive()


func end_game() -> void:
	main.TransitionScene(_ending)


func reset_stats() -> void:
	hp = hp_max
	mp = mp_max


func level_up() -> void:
	if dive_level < 6:
		dive_level += 1
		hp_max += 50
		mp_max += 100


func hp_drain(amount : int) -> void:
	if hp > 0:
		hp -= amount
		ui.update_stats()
	
	if hp <= 0:
		_current_dive.end_dive(2)


func mp_drain() -> void:
	if mp > 0:
		mp -= 1
		ui.update_stats()
	else:
		_current_dive.end_dive(3)


func assign_friend_memories() -> void:
	$World/NPCs/Charles.memory = preload("res://Scenes/Memories/Stories/CharlesStory.tscn")
	$World/NPCs/Evonn.memory = preload("res://Scenes/Memories/Stories/EvonnStory.tscn")
	$World/NPCs/Rachel.memory = preload("res://Scenes/Memories/Stories/RachelStory.tscn")
	$World/NPCs/Trent.memory = preload("res://Scenes/Memories/Stories/TrentStory.tscn")
