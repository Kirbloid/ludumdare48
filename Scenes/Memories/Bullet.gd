extends KinematicBody2D

onready var game := get_node("/root/Main/Scenes/Game")

var velocity = Vector2()

func set_params(set_trajectory: Vector2):
	velocity = set_trajectory * game._current_dive.memory_speed


func _physics_process(delta):
	move_and_slide(velocity)


func _on_HitBox_body_entered(body):
	game.hp_drain(game._current_dive.memory_damage)
	queue_free()


func _on_Despawn_timeout():
	queue_free()
