extends Node2D

onready var game := get_node("/root/Main/Scenes/Game")
onready var ui := get_node("/root/Main/Scenes/Game/UI")
onready var animation_player = get_node("AnimationPlayer")
onready var player_diver = get_node("PlayerDive")
onready var bullet_spawners = get_node("BulletSpawners")
onready var bullet_holder = get_node("BulletHolder")

export var bullet : PackedScene 

var _memory_queue := []
var default_memory_id = 0
var current_memory_id = 0
var memory_to_end_on = 0
var memory_speed = 220
var memory_damage = 5

func _ready():
	$PlayerDive.set_visible(false)
	$BGLoop.set_visible(false)
	animation_player.play("TransitionIntoMemory")
	if game.dive_level >= 1 and game.player.get_npc() == 4:
		bullet = preload("res://Scenes/Memories/Bullet2.tscn")
		memory_speed + 20


func start_dive(memory_story : PackedScene) -> void:
	var story_instance = memory_story.instance()
	_memory_queue = story_instance.duplicate().story
	var current_dialog : Dictionary = _memory_queue[0]
	$FloatingMemory.text = current_dialog["Dialog"]


func next_memory() -> void:
	if _memory_queue.empty():
		end_dive(4) # Reached the end ID
	else:
		var current_dialog : Dictionary = _memory_queue[0]
		$FloatingMemory.text = current_dialog["Dialog"]
		animation_player.seek(0)
		animation_player.play("ScrollMemoryText")
		ui.update_dive_story(current_dialog["Dialog"])
		current_memory_id = current_dialog["MemoryID"]
		_memory_queue.pop_front()
		$MemoryTimer.start()
		memory_damage += 5
		memory_speed += 10
		$Firing.wait_time -= 0.1


func end_dive(memory_id) -> void:
	if memory_id == 101 || memory_id == 201 || memory_id == 301 || memory_id == 401 || memory_id == 501 || memory_id == 601 || memory_id == 777:
		$SuccessText.set_visible(true)
	stop_timers()
	for bullet in bullet_holder.get_children():
		bullet.queue_free()
	player_diver.yeet()
	animation_player.play("TransitionOutOfMemory")
	memory_to_end_on = memory_id


func capture_memory() -> void:
	if player_diver.can_move:
		end_dive(current_memory_id)


func stop_timers() -> void:
	$MemoryTimer.stop()
	$MPTimer.stop()
	$Firing.stop()


func fire_bullet() -> void:
	var fired_bullet = bullet.instance()
	bullet_holder.add_child(fired_bullet)
	var random_pos = game.rng.randi_range(0, 7)
	fired_bullet.position = $BulletSpawners.get_child(random_pos).global_position
	var target_position = (player_diver.position - fired_bullet.global_position).normalized()
	fired_bullet.set_params(target_position)


func _on_AnimationPlayer_animation_finished(anim_name):
	match anim_name:
		"TransitionIntoMemory":
			next_memory()
			animation_player.play("PlayerStartsDive")
			$PlayerDive.set_visible(true)
			$BGLoop.set_visible(true)
			$BGLoop.play("Intro")
			$MemoryTimer.start()
			$MPTimer.start()
			$Firing.start()
		"PlayerStartsDive":
			player_diver.can_move = true
			animation_player.play("ScrollMemoryText", -1, 1.1)
		"TransitionOutOfMemory":
			game.end_dive(memory_to_end_on)


func _on_BGLoop_animation_finished():
	if $BGLoop.animation == "Intro":
		$BGLoop.play("Loop")


func _on_MemoryTimer_timeout():
	next_memory()


func _on_MPTimer_timeout():
	game.mp_drain()


func _on_Firing_timeout():
	fire_bullet()
