extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 0,
		"Dialog": "It's always great to see Leslea again. Hell, it's " + \
			"wonderful to see any of the crew back together again."
	},
	{
		"MemoryID": 0,
		"Dialog": "Leslea was... IS... something special. " + \
			"I'm a nobody."
	},
	{
		"MemoryID": 0,
		"Dialog": "I always assumed Trent was the biggest drinker. Turns " + \
			"out we both are, he's just better at holding his liquor."
	},
	{
		"MemoryID": 0,
		"Dialog": ". . . . ."
	},
	{
		"MemoryID": 0,
		"Dialog": "I always wanted to ask Leslea out. I don't know why " + \
			"this night of all nights was when I was an ass about it."
	},
	{
		"MemoryID": 501,
		"Dialog": "I didn't mean to make Leslea uncomfortable, but they're " + \
			"strong, and told me right off. Trent however, that asshole. " + \
			"Was liquored up and ready to use any excuse to beat the shit " + \
			"out of me. I took the blows. I deserved them.\n\n " + \
			"As I was laying on the ground, I saw Leslea walking over " + \
			"to the nearby stream."
	},
	{
		"MemoryID": 0,
		"Dialog": "We didn't here or see Leslea for a little bit and went looking."
	},
	{
		"MemoryID": 601,
		"Dialog": "We all found Leslea collapsed by the stream, breathing, " + \
			"but in a lot of pain. We had no idea what was happening. Help " + \
			"came quicker than we could have hoped for.  " + \
			"\n\n " + \
			"They said they were bitten. We didn't see anything. " + \
			"Please help them, doctor. "
	}
]
