extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 0,
		"Dialog": "I only came because I wanted to hang out with Leslea " + \
			"and Rachel."
	},
	{
		"MemoryID": 0,
		"Dialog": "Everyone knew I was leaving soon, so we were going " + \
			"to have a big going-away party next week."
	},
	{
		"MemoryID": 0,
		"Dialog": "That night, free of alchohol, I cried more on a nearby " + \
			"stump than I ever did sad-drunk."
	},
	{
		"MemoryID": 301,
		"Dialog": "Leslea stopped drinking early on to make sure I was okay. " + \
			"I wasn't. I wanted to stay, stay with Leslea and my small " + \
			"group of friends. Work was pulling me away and in my desperation " + \
			"I asked Leslea to come with. " + \
			"\n\n " + \
			"Of course they couldn't. Their own career, you know? " + \
			"Plus there's always online, and a short flight. It'll be fine, " + \
			"Leslea reassured me. Evonn was also looking for an excuse to join us..."
	},
	{
		"MemoryID": 0,
		"Dialog": "Long distance. Just isn't the same."
	},
	{
		"MemoryID": 0,
		"Dialog": "At least we could still play card games online, I told myself."
	},
	{
		"MemoryID": 0,
		"Dialog": "We didn't here or see Leslea for a little bit and went looking."
	},
	{
		"MemoryID": 601,
		"Dialog": "We all found Leslea collapsed by the stream, breathing, " + \
			"but in a lot of pain. We had no idea what was happening. Help " + \
			"came quicker than we could have hoped for.  " + \
			"\n\n " + \
			"They said they were bitten. We didn't see anything. " + \
			"Please help them, doctor. "
	}
]
