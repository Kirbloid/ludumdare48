extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 0,
		"Dialog": "I'm pretty I was THE reason Leslea got into sports, " + \
			"and turns out, might have been the reason they got out."
	},
	{
		"MemoryID": 0,
		"Dialog": "Water under the bridge you know? Anyways, the gathering " + \
			"tonight was all about getting together and remembering the good " + \
			"times."
	},
	{
		"MemoryID": 201,
		"Dialog": "I didn't think there was anything out of the ordinary that " + \
			"night, and truthfully, there wasn't at the time. We drank all " + \
			"the time at this spot. Rachel and Charles were already joining " + \
			"me, so it was pretty easy to have Leslea join in. Evonn was " + \
			"the only one not drinking."
	},
	{
		"MemoryID": 0,
		"Dialog": "I consider myself a pretty stand-up dude. I would have " + \
			"done anything to protect anyone tonight."
	},
	{
		"MemoryID": 0,
		"Dialog": "Goddamn Charles..."
	},
	{
		"MemoryID": 0,
		"Dialog": "Wow Charles I really messed you up, huh?"
	},
	{
		"MemoryID": 0,
		"Dialog": "We didn't here or see Leslea for a little bit and went looking."
	},
	{
		"MemoryID": 601,
		"Dialog": "We all found Leslea collapsed by the stream, breathing, " + \
			"but in a lot of pain. We had no idea what was happening. Help " + \
			"came quicker than we could have hoped for.  " + \
			"\n\n " + \
			"They said they were bitten. We didn't see anything. " + \
			"Please help them, doctor. "
	}
]
