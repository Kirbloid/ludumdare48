extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 500,
		"Dialog": "I hate the bugs buzzing around this time of year. " + \
			"The fire and a bit of spray keeps them away, but the " + \
			"occasional bite is a reminder of how much I don't " + \
			"enjoy the great outdoors. "
	},
	{
		"MemoryID": 101,
		"Dialog": "I didn't even want to go to this party in the first " + \
			"place, but Evonn was going to be there, and Trent, as stubborn " + \
			"as ever, was also pretty convincing."
	},
	{
		"MemoryID": 0,
		"Dialog": "Honestly, I missed them all. It was good to see them " + \
			"again..."
	},
	{
		"MemoryID": 0,
		"Dialog": "I don't miss the hangovers, that's for sure, and I " + \
			"certainly don't miss him when he's drunk. "
	},
	{
		"MemoryID": 0,
		"Dialog": "Maybe I should leave, too. Yeah, next spring, when " + \
			"it's not so damn muggy. Ugh our winters suck, too. " + \
			"I feel trapped in this state. "
	},
	{
		"MemoryID": 0,
		"Dialog": "He makes me so uncomfortable, why do I hang out with " + \
			"him? What does he see in me, anyways?"
	},
	{
		"MemoryID": 0,
		"Dialog": "Charles always goes too far. Trent means well, and Rachel " + \
			"tries to keep a positive outlook but turns a blind eye. Evonn is soooo done with " + \
			"all this shit. I don't blame her. " + \
			"\n\n" + \
			"Screw those guys, just take me with you, Evonn. "
	},
	{
		"MemoryID": 0,
		"Dialog": "Being by myself isn't too bad either. I could just jump " + \
			"in the stream right now and wash away all my worries. Well, " + \
			"and wash away my phone, and a few hundred dollars paid into it."
	},
	{
		"MemoryID": 0,
		"Dialog": "Startled by something out of the corner of my eye, " + \
			"I tripped and fell. I thought I had hit a rock but the " + \
			"mud was a cushion enough. "
	},
	{
		"MemoryID": 777,
		"Dialog": "It was dark and I could barely see, but I could hear " + \
			"it rattling right before the moment it struck. The last thing I " + \
			"remember was crying out and catching one final look. \n\n" + \
			"\n " + \
			"In a flicker of moonlight through the trees I saw a diamond " + \
			"pattern before its black and white bands slithered out of view..."
	},
	{
		"MemoryID": 777,
		"Dialog": "... a diamond " + \
			"pattern with black and white bands..."
	},
	{
		"MemoryID": 777,
		"Dialog": "... a rattle at the tip of the tail..."
	}
]
