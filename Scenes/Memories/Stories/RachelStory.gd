extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 0,
		"Dialog": "I was kinda drunk and don't remember much of the night."
	},
	{
		"MemoryID": 0,
		"Dialog": "Let's see. Fuck Charles. That's all."
	},
	{
		"MemoryID": 0,
		"Dialog": "I wish I got to know Leslea more."
	},
	{
		"MemoryID": 0,
		"Dialog": "Evonn helped get me where I am today. Great life " + \
			"coach, and certainly helped me get a grip on my finances. Going to miss them."
	},
	{
		"MemoryID": 401,
		"Dialog": "So Charles started flirting up a storm. Fucking drunks, " + \
			"I swear. Trent didn't mean to but was egging the whole thing " + \
			"on, and when I told him to stop he got unusually chivalrous. " + \
			"\n\n " + \
			"Next thing we knew, Charles starts hitting on Leslea, but the " + \
			"kind of hitting on that only beer can fuel, and he would not " + \
			"let up. "
	},
	{
		"MemoryID": 0,
		"Dialog": "Trent's pretty hot when he's fighting."
	},
	{
		"MemoryID": 0,
		"Dialog": "We didn't here or see Leslea for a little bit and went looking."
	},
	{
		"MemoryID": 601,
		"Dialog": "We all found Leslea collapsed by the stream, breathing, " + \
			"but in a lot of pain. We had no idea what was happening. Help " + \
			"came quicker than we could have hoped for.  " + \
			"\n\n " + \
			"They said they were bitten. We didn't see anything. " + \
			"Please help them, doctor. "
	}
]
