extends Node2D

export var default_memory_id : int

var story = [
	{
		"MemoryID": 0,
		"Dialog": "This is the memory story dialog. A memory is now in " + \
			"progress, and as an added bonus, these memories will start " + \
			"attacking you!"
	},
	{
		"MemoryID": 1001,
		"Dialog": "This is a memory that should be captured."
	},
	{
		"MemoryID": 0,
		"Dialog": "Lorem ipsum dolor sit amet, consectetur adipiscing " + \
			"elit. Nunc porta convallis orci, vel venenatis tortor vulputate " + \
			"accumsan. Donec imperdiet odio vel dictum tempus. " + \
			"Duis elementum vitae nisl vel pellentesque. Fusce turpis " + \
			"sapien, congue a porta vel, vulputate et sapien. Ut ac metus " + \
			"eget dui tempor malesuada vitae nec diam. Aenean eu quam " + \
			"maximus, luctus nisl quis, lobortis sem. Curabitur efficitur " + \
			"arcu nec vehicula semper. Vestibulum!"
	}
]
