extends KinematicBody2D

const SPEED : float = 25.0
const MAXACCEL : float = 100.0
const ACCEL_RAMP : float = 5.0
const FRICTION_RAMP : float = 10.0

onready var game := get_node("/root/Main/Scenes/Game")
onready var sprite := get_node("Sprite")

var velocity := Vector2()
var acceleration_velocity := Vector2()
var moving_x := false
var moving_y := false

var _current_npc

func _ready() -> void:
	pass


func _process(delta) -> void:
	friction()
	button_prompts()


func _physics_process(delta) -> void:
	move_and_slide(velocity + acceleration_velocity)


func stop() -> void:
	velocity = Vector2()
	acceleration_velocity = Vector2()


func friction() -> void:
	if moving_x == false:
		if (acceleration_velocity.x < 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x + FRICTION_RAMP, -MAXACCEL, 0)
		if (acceleration_velocity.x > 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x - FRICTION_RAMP, 0, MAXACCEL)
			
	if moving_y == false:
		if (acceleration_velocity.y < 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y + FRICTION_RAMP, -MAXACCEL, 0)
		if (acceleration_velocity.y > 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y - FRICTION_RAMP, 0, MAXACCEL)


func button_prompts():
	if get_talk_interaction() == null || not game.get_state() == game.State.WALKING:
		$ZPrompt.set_visible(false)
	else:
		$ZPrompt.set_visible(true)
	
	if get_dive_interaction() == null || not game.get_state() == game.State.WALKING:
		$XPrompt.set_visible(false)
	else:
		$XPrompt.set_visible(true)


func move_right() -> void:
	sprite.flip_h = true
	moving_x = true
	velocity.x += SPEED
	acceleration_velocity.x = clamp(acceleration_velocity.x + ACCEL_RAMP, 0, MAXACCEL)


func move_left() -> void:
	sprite.flip_h = false
	moving_x = true
	velocity.x -= SPEED
	acceleration_velocity.x = clamp(acceleration_velocity.x - ACCEL_RAMP, -MAXACCEL, 0)


func move_down() -> void:
	moving_y = true
	velocity.y += SPEED
	acceleration_velocity.y = clamp(acceleration_velocity.y + ACCEL_RAMP, 0, MAXACCEL)


func move_up() -> void:
	moving_y = true
	velocity.y -= SPEED
	acceleration_velocity.y = clamp(acceleration_velocity.y - ACCEL_RAMP, -MAXACCEL, 0)


func get_talk_interaction() -> PackedScene:
	if not _current_npc == null:
		return _current_npc.get_dialog()
	else:
		return null


func get_dive_interaction() -> PackedScene:
	if not _current_npc == null:
		return _current_npc.get_memory()
	else:
		return null


func get_npc() -> int:
	return _current_npc.npc


func _on_NPCInteract_body_entered(body):
	_current_npc = body


func _on_NPCInteract_body_exited(body):
	_current_npc = null
