extends Control

onready var main = get_node("/root/Main")
var can_click = false


func _ready():
	pass


func _process(delta):
	if Input.is_action_just_pressed("z") and can_click:
		main.TransitionScene(main._title_screen)
	elif Input.is_action_just_pressed("x") and can_click:
		main.TransitionScene(main._title_screen)


func _on_Click_timeout():
	can_click = true
