extends KinematicBody2D

const SPEED : float = 150.0
const MAXDASH : float = 750.0
const MAXACCEL : float = 150.0
const ACCEL_RAMP : float = 5.0
const FRICTION_RAMP : float = 20.0

onready var sprite := get_node("Sprite")
onready var game := get_node("/root/Main/Scenes/Game")

var velocity := Vector2()
var dash_velocity := Vector2()
var acceleration_velocity := Vector2()
var moving_x := false
var moving_y := false
var can_move = false


func _ready() -> void:
	pass


func _process(delta) -> void:
	friction()


func _physics_process(delta) -> void:
	move_and_slide(velocity + acceleration_velocity + dash_velocity)


func friction() -> void:
	if (dash_velocity.x < 0):
		dash_velocity.x = clamp(dash_velocity.x + FRICTION_RAMP, -MAXDASH, 0)
	if (dash_velocity.x > 0):
		dash_velocity.x = clamp(dash_velocity.x - FRICTION_RAMP, 0, MAXDASH)
	
	if (dash_velocity.y < 0):
		dash_velocity.y = clamp(dash_velocity.y + FRICTION_RAMP, -MAXDASH, 0)
	if (dash_velocity.y > 0):
		dash_velocity.y = clamp(dash_velocity.y - FRICTION_RAMP, 0, MAXDASH)
		
	if moving_x == false:
		if (acceleration_velocity.x < 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x + FRICTION_RAMP / 3, -MAXACCEL, 0)
		if (acceleration_velocity.x > 0):
			acceleration_velocity.x = clamp(acceleration_velocity.x - FRICTION_RAMP / 3, 0, MAXACCEL)
			
	if moving_y == false:
		if (acceleration_velocity.y < 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y + FRICTION_RAMP / 3, -MAXACCEL, 0)
		if (acceleration_velocity.y > 0):
			acceleration_velocity.y = clamp(acceleration_velocity.y - FRICTION_RAMP / 3, 0, MAXACCEL)


func move_right() -> void:
	if can_move:
		sprite.flip_h = false
		moving_x = true
		velocity.x += SPEED
		acceleration_velocity.x = clamp(acceleration_velocity.x + ACCEL_RAMP, 0, MAXACCEL)


func move_left() -> void:
	if can_move:
		sprite.flip_h = true
		moving_x = true
		velocity.x -= SPEED
		acceleration_velocity.x = clamp(acceleration_velocity.x - ACCEL_RAMP, -MAXACCEL, 0)


func move_down() -> void:
	if can_move:
		moving_y = true
		velocity.y += SPEED
		acceleration_velocity.y = clamp(acceleration_velocity.y + ACCEL_RAMP, 0, MAXACCEL)


func move_up() -> void:
	if can_move:
		moving_y = true
		velocity.y -= SPEED
		acceleration_velocity.y = clamp(acceleration_velocity.y - ACCEL_RAMP, -MAXACCEL, 0)


func dash() -> void:
	if can_move:
		game.mp_drain()
		dash_velocity = velocity * 3.5


func yeet() -> void:
	can_move = false
	$AnimationPlayer.play("Yeet")
