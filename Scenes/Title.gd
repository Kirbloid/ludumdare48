extends Control

export var screens : Array

onready var main = get_node("/root/Main")
var intro = 0


func _ready():
	screens.push_back($TitleScreen)
	screens.push_back($Intro1)
	screens.push_back($Intro2)
	screens.push_back($Intro3)


func _process(delta):
	if Input.is_action_just_pressed("z"):
		main.TransitionScene(main._game)
		if intro == 3:
			main.TransitionScene(main._game)
		else:
			intro += 1
			#screens[intro].show()
	elif Input.is_action_just_pressed("x"):
		main.TransitionScene(main._game)
