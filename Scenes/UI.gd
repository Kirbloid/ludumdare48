extends CanvasLayer

enum NPC { CHARLES, EVONN, RACHEL, TRENT, LESLEA }

onready var game := get_node("/root/Main/Scenes/Game")
onready var player := get_node("/root/Main/Scenes/Game")
onready var player_portrait = get_node("Dialog/Player")
onready var npc_portraits = get_node("Dialog/NPCPortraits")
onready var narrative = get_node("Dialog/Narrative")

# stats
onready var hp_label = get_node("PlayerUI/Stats/HBoxHP/ProgressBar/Current")
onready var hp_bar = get_node("PlayerUI/Stats/HBoxHP/ProgressBar")
onready var mp_label = get_node("PlayerUI/Stats/HBoxMP/ProgressBar/Current")
onready var mp_bar = get_node("PlayerUI/Stats/HBoxMP/ProgressBar")
onready var dive_level = get_node("PlayerUI/Stats/HBoxDive/ProgressBar")
const DIVE_LEVEL_MAX = 6

var current_npc : int = NPC.LESLEA

var _dialog_queue := []
var default_thoughts := "This is it. This is the only shot we got. Hold on Leslea, " + \
"I'm going to help however I can. Charles, Evonn, Rachel, and Trent... you're " + \
"all going to help even if you don't realize it."


func _ready() -> void:
	set_default_thoughts()


func set_default_thoughts() -> void:
	narrative.text = default_thoughts
	player_portrait.modulate.a = 0.5
	npc_portraits.modulate.a = 0


func update_default_thoughts(thoughts : String) -> void:
	default_thoughts = thoughts


func start_dialog(dialog: Array, npc : int) -> void:
	npc_portraits.set_visible(true)
	for portrait in npc_portraits.get_children():
		portrait.set_visible(false)
		npc_portraits.get_child(npc).set_visible(true)
	_dialog_queue = dialog.duplicate()
	update_dialog()


func update_dialog() -> void:
	var current_dialog : Dictionary = _dialog_queue[0]
	
	if current_dialog["Player"] == true:
		player_portrait.modulate.a = 0.5
		npc_portraits.modulate.a = 0.15
	else:
		player_portrait.modulate.a = 0.15
		npc_portraits.modulate.a = 0.5
	
	narrative.text = current_dialog["Dialog"]
	
	_dialog_queue.pop_front()
	
	$Dialog/Cont.set_visible(false)
	$Dialog/End.set_visible(false)
	if _dialog_queue.empty():
		$Dialog/End.set_visible(true)
	else:
		$Dialog/Cont.set_visible(true)


func advance_dialog() -> void:
	if _dialog_queue.empty():
		end_dialog()
	else:
		update_dialog()


func end_dialog() -> void:
	game.set_state(game.State.WALKING)
	npc_portraits.set_visible(false)
	set_default_thoughts()
	$Dialog/Cont.set_visible(false)
	$Dialog/End.set_visible(false)


func start_dive(npc : int) -> void:
	current_npc = npc
	npc_portraits.set_visible(true)
	for portrait in npc_portraits.get_children():
		portrait.set_visible(false)
		npc_portraits.get_child(npc).set_visible(true)
	npc_portraits.modulate.a = 0.15
	player_portrait.set_visible(false)
	$Dialog/Dash.set_visible(true)
	$Dialog/Capture.set_visible(true)
	update_dive_story("")


func update_dive_story(story: String) -> void:
	narrative.text = story


func end_dive() -> void:
	$Dialog/Dash.set_visible(false)
	$Dialog/Capture.set_visible(false)
	player_portrait.set_visible(true)
	update_stats()


func update_stats() -> void:
	hp_label.text = str(game.hp)
	mp_label.text = str(game.mp)
	hp_bar.set_max(game.hp_max)
	hp_bar.set_value(game.hp)
	mp_bar.set_max(game.mp_max)
	mp_bar.set_value(game.mp)
	dive_level.set_value(game.dive_level)
