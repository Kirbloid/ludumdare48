extends StaticBody2D

enum NPC { CHARLES, EVONN, RACHEL, TRENT, LESLEA }

export var npc_name : String
export var dialog : PackedScene
export var memory : PackedScene
export(NPC) var npc


func _ready():
	pass


func get_dialog() -> PackedScene:
	return dialog


func get_memory() -> PackedScene:
	return memory
