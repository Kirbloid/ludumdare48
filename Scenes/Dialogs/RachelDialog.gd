extends Node2D

var story = [
	{
		"Player": true,
		"Dialog": "What's wrong, Rachel? "
	},
	{
		"Player": false,
		"Dialog": "I swear we weren't doing anything illegal this is all " + \
			"just... just... "
	},
	{
		"Player": true,
		"Dialog": "I'm not a cop. I'm just here to actually help."
	}
]
