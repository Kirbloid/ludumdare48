extends Node2D

var story = [
	{
		"Player": true,
		"Dialog": "Hey, Trent. "
	},
	{
		"Player": false,
		"Dialog": "I'm so sorry I didn't mean to... I'm just... I'm..."
	},
	{
		"Player": true,
		"Dialog": "Hey, it's fine, we'll make it through this, okay?"
	},
	{
		"Player": false,
		"Dialog": "Yeah. Okay."
	}
]
