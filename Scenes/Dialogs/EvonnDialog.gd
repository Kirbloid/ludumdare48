extends Node2D

var story = [
	{
		"Player": true,
		"Dialog": "Evonn, how are you holding up? "
	},
	{
		"Player": false,
		"Dialog": "Is Leslea... "
	},
	{
		"Player": true,
		"Dialog": "I'm sorry I don't know how to explain... "
	},
	{
		"Player": false,
		"Dialog": "Naw, it's okay. Do what you need to do."
	}
]
