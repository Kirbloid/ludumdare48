extends Node2D

var story = [
	{
		"Player": true,
		"Dialog": "I am testing the field dialog!"
	},
	{
		"Player": false,
		"Dialog": "That is very good. Good job testing the field dialog."
	},
	{
		"Player": true,
		"Dialog": "Mind giving me some of that Lorum Ipsum?"
	},
	{
		"Player": false,
		"Dialog": "Fine..."
	},
	{
		"Player": false,
		"Dialog": "Lorem ipsum dolor sit amet, consectetur adipiscing " + \
			"elit. Nunc porta convallis orci, vel venenatis tortor vulputate " + \
			"accumsan. Donec imperdiet odio vel dictum tempus. " + \
			"Duis elementum vitae nisl vel pellentesque. Fusce turpis " + \
			"sapien, congue a porta vel, vulputate et sapien. Ut ac metus " + \
			"eget dui tempor malesuada vitae nec diam. Aenean eu quam " + \
			"maximus, luctus nisl quis, lobortis sem. Curabitur efficitur " + \
			"arcu nec vehicula semper. Vestibulum!"
	}
]
