extends Node2D

var story = [
	{
		"Player": true,
		"Dialog": "You're doing great, Charles. "
	},
	{
		"Player": false,
		"Dialog": "I'll do anything I can to help save Leslea. Please, " + \
			"anything I can do to make things right... "
	},
	{
		"Player": true,
		"Dialog": "You being here is enough. (Even if you all aren't real " + \
			"at the moment, you're more useful this way.)"
	}
]
