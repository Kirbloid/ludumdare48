6 level up memories...

Leslea
1) So... Leslea and their friends went camping over the weekend.

Trent
2) Trent convinces Leslea to drink with him, Charles, and Rachel, but Evonn is sitting out.

Evonn
3) Leslea slows down with the drinking to console Evonn who's moving away. Rachel joins them.

Rachel
4) Rachel teases Charles is saying too much and making her uncomfortable, Trent egging him on. Reveals Charles started hitting on Leslea.

Charles
5) Charles pushes too far, and Trent, defending Leslea, gets in a fight with him. Leslea is last seen walking off to a nearby stream to let them cool down.

Anyone but Leslea
6) The group found Leslea collapsed nearby, the doctors discovering bite marks on her ankle.

FINAL) Where she was bit by a Western diamonback rattlesnake...

