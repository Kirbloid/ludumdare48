extends Node2D

var _title_screen = preload("res://Scenes/Title.tscn")
var _game = preload("res://Scenes/Game.tscn")

var next_scene
var current_scene
var ending = 0


func _ready():
	current_scene = _title_screen.instance()
	next_scene = _title_screen
	ChangeScene()


func TransitionScene(scene):
	next_scene = scene
	$FadeAnimations.play("FadeOut")


func ChangeScene():
	current_scene.queue_free()
	current_scene = next_scene.instance()
	$Scenes.add_child(current_scene)


func _on_FadeAnimations_animation_finished(anim_name):
	if anim_name == "FadeOut":
		ChangeScene()
		$FadeAnimations.play("FadeIn")
